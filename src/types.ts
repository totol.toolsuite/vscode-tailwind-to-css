export interface Entries {
  [key: string]: string;
}

export interface Tokens {
  [key: string]: string;
}