// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { convertTailwindToCSS } from './converter';
import { generateTableContent } from './doc';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Register the command to convert Tailwind CSS to CSS
    let converterFunction = vscode.commands.registerCommand('tailwind-to-css.convertTailwindToCSS', () => {
        // Get the active text editor
        const editor = vscode.window.activeTextEditor;
        if (editor) {
            // Get the current selection or the current line if no selection is made
            let selection = editor.selection;
            let text = '';
            if (selection.isEmpty) {
                // If no text is selected, use the whole line
                const lineNumber = selection.active.line;
                const line = editor.document.lineAt(lineNumber);
                const startPos = new vscode.Position(lineNumber, 0);
                const endPos = new vscode.Position(lineNumber, line.range.end.character);
                selection = new vscode.Selection(startPos, endPos);
                text = line.text;
            } else {
                // If text is selected, use the selected text
                text = editor.document.getText(selection);
            }

            // Convert Tailwind CSS to CSS
            let convertedCSS = convertTailwindToCSS(text);

            // Replace the selected text with the converted CSS
            editor.edit(editBuilder => {
                editBuilder.replace(selection, '\t' + convertedCSS);
            });
        }
    });

    context.subscriptions.push(converterFunction);

    let disposable = vscode.commands.registerCommand('tailwind-to-css.availableShortcuts', () => {
        const panel = vscode.window.createWebviewPanel(
            'tablePreview', // Identifies the type of the webview. Used internally
            '[Tailwind to CSS] - Shortcuts list', // Title of the panel displayed to the user
            vscode.ViewColumn.One, // Editor column to show the new webview panel in.
            {} // Webview options. More on these later.
        );
        panel.webview.html = generateTableContent();

        context.subscriptions.push(disposable);
    });
}

// This method is called when your extension is deactivated
export function deactivate() {}
