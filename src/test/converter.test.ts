import * as assert from 'assert';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
import { convertTailwindToCSS } from '../converter';

suite('Extension Test Suite', () => {
	vscode.window.showInformationMessage('Start all tests.');

	test('convert a rule', () => {
    const converted = convertTailwindToCSS('flex');
		assert.strictEqual(converted, 'display: flex;');
	});

	test('convert multiple rules', () => {
    const converted = convertTailwindToCSS('flex absolute top-4');
		assert.strictEqual(converted, 'display: flex;\n\tposition: absolute;\n\ttop: 1rem;');
	});
});
