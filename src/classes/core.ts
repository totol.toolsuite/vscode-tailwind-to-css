export const coreClassesMap: Record<string, string> = {
  'float-right':  'float: right',
  'pointer-events-none': 'pointer-events: none',
  'pointer-events-auto': 'pointer-events: auto',
  'visible': 'visibility: visible',
  'invisible': 'visibility: hidden',
  'collapse': 'visibility: collapse',
  'static': 'position: static',
  'fixed': 'position: fixed',
  'absolute': 'position: absolute',
  'relative': 'position: relative',
  'sticky': 'position: sticky',
  'float-start': 'float: inline-start',
  'float-end': 'float: inline-end',
  'float-left':  'float: left',
  'float-none':  'float: none',
  'clear-start': 'clear: inline-start',
  'clear-end': 'clear: inline-end',
  'clear-left': 'clear: left',
  'clear-right': 'clear: right',
  'clear-both': 'clear: both',
  'clear-none': 'clear: none',
  'box-border': 'box-sizing: border-box',
  'box-content': 'box-sizing: content-box',
  'block': 'display: block',
  'inline-block': 'display: inline-block',
  'inline': 'display: inline',
  'flex': 'display: flex',
  'inline-flex': 'display: inline-flex',
  'table': 'display: table',
  'inline-table': 'display: inline-table',
  'table-caption': 'display: table-caption',
  'table-cell': 'display: table-cell',
  'table-column': 'display: table-column',
  'table-column-group': 'display: table-column-group',
  'table-footer-group': 'display: table-footer-group',
  'table-header-group': 'display: table-header-group',
  'table-row-group': 'display: table-row-group',
  'table-row': 'display: table-row',
  'flow-root': 'display: flow-root',
  'grid': 'display: grid',
  'inline-grid': 'display: inline-grid',
  'contents': 'display: contents',
  'list-item': 'display: list-item',
  'hidden': 'display: none',
  'table-auto': 'table-layout: auto',
  'table-fixed': 'table-layout: fixed',
  'caption-top': 'caption-side: top',
  'caption-bottom': 'caption-side: bottom',
  'isolate': 'isolation: isolate',
  'isolation-auto': 'isolation: auto',
  'border-collapse': 'border-collapse: collapse',
  'border-separate': 'border-collapse: separate',
  'select-none': 'user-select: none',
  'select-text': 'user-select: text',
  'select-all': 'user-select: all',
  'select-auto': 'user-select: auto',
  'resize-none': 'resize: none',
  'resize-y': 'resize: vertical',
  'resize-x': 'resize: horizontal',
  'resize': 'resize: both',
  'snap-none': 'scroll-snap-type: none',
  'snap-start': 'scroll-snap-align: start',
  'snap-end': 'scroll-snap-align: end',
  'snap-center': 'scroll-snap-align: center',
  'snap-align-none': 'scroll-snap-align: none',
  'snap-normal': 'scroll-snap-stop: normal',
  'snap-always': 'scroll-snap-stop: always',
  'list-inside': 'list-style-position: inside',
  'list-outside': 'list-style-position: outside',
  'appearance-none': 'appearance: none',
  'appearance-auto': 'appearance: auto',
  'break-before-auto': 'break-before: auto',
  'break-before-avoid': 'break-before: avoid',
  'break-before-all': 'break-before: all',
  'break-before-avoid-page': 'break-before: avoid-page',
  'break-before-page': 'break-before: page',
  'break-before-left': 'break-before: left',
  'break-before-right': 'break-before: right',
  'break-before-column': 'break-before: column',
  'break-inside-auto': 'break-inside: auto',
  'break-inside-avoid': 'break-inside: avoid',
  'break-inside-avoid-page': 'break-inside: avoid-page',
  'break-inside-avoid-column': 'break-inside: avoid-column',
  'break-after-auto': 'break-after: auto',
  'break-after-avoid': 'break-after: avoid',
  'break-after-all': 'break-after: all',
  'break-after-avoid-page': 'break-after: avoid-page',
  'break-after-page': 'break-after: page',
  'break-after-left': 'break-after: left',
  'break-after-right': 'break-after: right',
  'break-after-column': 'break-after: column',
  'grid-flow-row': 'grid-auto-flow: row',
  'grid-flow-col': 'grid-auto-flow: column',
  'grid-flow-dense': 'grid-auto-flow: dense',
  'grid-flow-row-dense': 'grid-auto-flow: row dense',
  'grid-flow-col-dense': 'grid-auto-flow: column dense',
  'flex-row': 'flex-direction: row',
  'flex-row-reverse': 'flex-direction: row-reverse',
  'flex-col': 'flex-direction: column',
  'flex-col-reverse': 'flex-direction: column-reverse',
  'flex-wrap': 'flex-wrap: wrap',
  'flex-wrap-reverse': 'flex-wrap: wrap-reverse',
  'flex-nowrap': 'flex-wrap: nowrap',
  'place-content-center': 'place-content: center',
  'place-content-start': 'place-content: start',
  'place-content-end': 'place-content: end',
  'place-content-between': 'place-content: space-between',
  'place-content-around': 'place-content: space-around',
  'place-content-evenly': 'place-content: space-evenly',
  'place-content-baseline': 'place-content: baseline',
  'place-content-stretch': 'place-content: stretch',
  'place-items-start': 'place-items: start',
  'place-items-end': 'place-items: end',
  'place-items-center': 'place-items: center',
  'place-items-baseline': 'place-items: baseline',
  'place-items-stretch': 'place-items: stretch',
  'content-normal': 'align-content: normal',
  'content-center': 'align-content: center',
  'content-start': 'align-content: flex-start',
  'content-end': 'align-content: flex-end',
  'content-between': 'align-content: space-between',
  'content-around': 'align-content: space-around',
  'content-evenly': 'align-content: space-evenly',
  'content-baseline': 'align-content: baseline',
  'content-stretch': 'align-content: stretch',
  'items-start': 'align-items: flex-start',
  'items-end': 'align-items: flex-end',
  'items-center': 'align-items: center',
  'items-baseline': 'align-items: baseline',
  'items-stretch': 'align-items: stretch',
  'justify-normal': 'justify-content: normal',
  'justify-start': 'justify-content: flex-start',
  'justify-end': 'justify-content: flex-end',
  'justify-center': 'justify-content: center',
  'justify-between': 'justify-content: space-between',
  'justify-around': 'justify-content: space-around',
  'justify-evenly': 'justify-content: space-evenly',
  'justify-stretch': 'justify-content: stretch',
  'justify-items-start': 'justify-items: start',
  'justify-items-end': 'justify-items: end',
  'justify-items-center': 'justify-items: center',
  'justify-items-stretch': 'justify-items: stretch',
  'place-self-auto': 'place-self: auto',
  'place-self-start': 'place-self: start',
  'place-self-end': 'place-self: end',
  'place-self-center': 'place-self: center',
  'place-self-stretch': 'place-self: stretch',
  'self-auto': 'align-self: auto',
  'self-start': 'align-self: flex-start',
  'self-end': 'align-self: flex-end',
  'self-center': 'align-self: center',
  'self-stretch': 'align-self: stretch',
  'self-baseline': 'align-self: baseline',
  'justify-self-auto': 'justify-self: auto',
  'justify-self-start': 'justify-self: start',
  'justify-self-end': 'justify-self: end',
  'justify-self-center': 'justify-self: center',
  'justify-self-stretch': 'justify-self: stretch',
  'overflow-auto': 'overflow: auto',
  'overflow-hidden': 'overflow: hidden',
  'overflow-clip': 'overflow: clip',
  'overflow-visible': 'overflow: visible',
  'overflow-scroll': 'overflow: scroll',
  'overflow-x-auto': 'overflow-x: auto',
  'overflow-y-auto': 'overflow-y: auto',
  'overflow-x-hidden': 'overflow-x: hidden',
  'overflow-y-hidden': 'overflow-y: hidden',
  'overflow-x-clip': 'overflow-x: clip',
  'overflow-y-clip': 'overflow-y: clip',
  'overflow-x-visible': 'overflow-x: visible',
  'overflow-y-visible': 'overflow-y: visible',
  'overflow-x-scroll': 'overflow-x: scroll',
  'overflow-y-scroll': 'overflow-y: scroll',
  'overscroll-auto': 'overscroll-behavior: auto',
  'overscroll-contain': 'overscroll-behavior: contain',
  'overscroll-none': 'overscroll-behavior: none',
  'overscroll-y-auto': 'overscroll-behavior-y: auto',
  'overscroll-y-contain': 'overscroll-behavior-y: contain',
  'overscroll-y-none': 'overscroll-behavior-y: none',
  'overscroll-x-auto': 'overscroll-behavior-x: auto',
  'overscroll-x-contain': 'overscroll-behavior-x: contain',
  'overscroll-x-none': 'overscroll-behavior-x: none',
  'scroll-auto': 'scroll-behavior: auto',
  'scroll-smooth': 'scroll-behavior: smooth',
  'truncate': 'overflow: hidden;text-overflow: ellipsis;white-space: nowrap',
  'text-ellipsis': 'text-overflow: ellipsis',
  'text-clip': 'text-overflow: clip',
  'hyphens-none': 'hyphens: none',
  'hyphens-manual': 'hyphens: manual',
  'hyphens-auto': 'hyphens: auto',
  'whitespace-normal': 'white-space: normal',
  'whitespace-nowrap': 'white-space: nowrap',
  'whitespace-pre': 'white-space: pre',
  'whitespace-pre-line': 'white-space: pre-line',
  'whitespace-pre-wrap': 'white-space: pre-wrap',
  'whitespace-break-spaces': 'white-space: break-spaces',
  'text-wrap': 'text-wrap: wrap',
  'text-nowrap': 'text-wrap: nowrap',
  'text-balance': 'text-wrap: balance',
  'text-pretty': 'text-wrap: pretty',
  'break-normal': 'overflow-wrap: normal;word-break: normal',
  'break-words': 'overflow-wrap: break-word',
  'break-all': 'word-break: break-all',
  'break-keep': 'word-break: keep-all',
  'border-solid': 'border-style: solid',
  'border-dashed': 'border-style: dashed',
  'border-dotted': 'border-style: dotted',
  'border-double': 'border-style: double',
  'border-hidden': 'border-style: hidden',
  'border-none': 'border-style: none',
  'box-decoration-slice': 'box-decoration-break: slice',
  'box-decoration-clone': 'box-decoration-break: clone',
  'bg-fixed': 'background-attachment: fixed',
  'bg-local': 'background-attachment: local',
  'bg-scroll': 'background-attachment: scroll',
  'bg-auto': 'background-size: auto',
  'bg-cover': 'background-size: cover',
  'bg-contain':	'background-size: contain',
  'bg-clip-border': 'background-clip: border-box',
  'bg-clip-padding': 'background-clip: padding-box',
  'bg-clip-content': 'background-clip: content-box',
  'bg-clip-text': 'background-clip: text',
  'bg-repeat': 'background-repeat: repeat',
  'bg-no-repeat': 'background-repeat: no-repeat',
  'bg-repeat-x': 'background-repeat: repeat-x',
  'bg-repeat-y': 'background-repeat: repeat-y',
  'bg-repeat-round': 'background-repeat: round',
  'bg-repeat-space': 'background-repeat: space',
  'bg-origin-border': 'background-origin: border-box',
  'bg-origin-padding': 'background-origin: padding-box',
  'bg-origin-content': 'background-origin: content-box',
  'object-contain': 'object-fit: contain',
  'object-cover': 'object-fit: cover',
  'object-fill': 'object-fit: fill',
  'object-none': 'object-fit: none',
  'object-scale-down': 'object-fit: scale-down',
  'text-left': 'text-align: left',
  'text-center': 'text-align: center',
  'text-right': 'text-align: right',
  'text-justify': 'text-align: justify',
  'text-start': 'text-align: start',
  'text-end': 'text-align: end',
  'align-baseline': 'vertical-align: baseline',
  'align-top': 'vertical-align: top',
  'align-middle': 'vertical-align: middle',
  'align-bottom': 'vertical-align: bottom',
  'align-text-top': 'vertical-align: text-top',
  'align-text-bottom': 'vertical-align: text-bottom',
  'align-sub': 'vertical-align: sub',
  'align-super': 'vertical-align: super',
  'uppercase': 'text-transform: uppercase',
  'lowercase': 'text-transform: lowercase',
  'capitalize': 'text-transform: capitalize',
  'normal-case': 'text-transform: none',
  'normal-nums': 'font-variant-numeric: normal',
  'underline': 'text-decoration-line: underline',
  'overline': 'text-decoration-line: overline',
  'line-through': 'text-decoration-line: line-through',
  'no-underline': 'text-decoration-line: none',
  'decoration-solid': 'text-decoration-style: solid',
  'decoration-double': 'text-decoration-style: double',
  'decoration-dotted': 'text-decoration-style: dotted',
  'decoration-dashed': 'text-decoration-style: dashed',
  'decoration-wavy': 'text-decoration-style: wavy',
  'bg-blend-normal': 'background-blend-mode: normal',
  'bg-blend-multiply': 'background-blend-mode: multiply',
  'bg-blend-screen': 'background-blend-mode: screen',
  'bg-blend-overlay': 'background-blend-mode: overlay',
  'bg-blend-darken': 'background-blend-mode: darken',
  'bg-blend-lighten': 'background-blend-mode: lighten',
  'bg-blend-color-dodge': 'background-blend-mode: color-dodge',
  'bg-blend-color-burn': 'background-blend-mode: color-burn',
  'bg-blend-hard-light': 'background-blend-mode: hard-light',
  'bg-blend-soft-light': 'background-blend-mode: soft-light',
  'bg-blend-difference': 'background-blend-mode: difference',
  'bg-blend-exclusion': 'background-blend-mode: exclusion',
  'bg-blend-hue': 'background-blend-mode: hue',
  'bg-blend-saturation': 'background-blend-mode: saturation',
  'bg-blend-color': 'background-blend-mode: color',
  'bg-blend-luminosity': 'background-blend-mode: luminosity',
  'mix-blend-normal': 'mix-blend-mode: normal',
  'mix-blend-multiply': 'mix-blend-mode: multiply',
  'mix-blend-screen': 'mix-blend-mode: screen',
  'mix-blend-overlay': 'mix-blend-mode: overlay',
  'mix-blend-darken': 'mix-blend-mode: darken',
  'mix-blend-lighten': 'mix-blend-mode: lighten',
  'mix-blend-color-dodge': 'mix-blend-mode: color-dodge',
  'mix-blend-color-burn': 'mix-blend-mode: color-burn',
  'mix-blend-hard-light': 'mix-blend-mode: hard-light',
  'mix-blend-soft-light': 'mix-blend-mode: soft-light',
  'mix-blend-difference': 'mix-blend-mode: difference',
  'mix-blend-exclusion': 'mix-blend-mode: exclusion',
  'mix-blend-hue': 'mix-blend-mode: hue',
  'mix-blend-saturation': 'mix-blend-mode: saturation',
  'mix-blend-color': 'mix-blend-mode: color',
  'mix-blend-luminosity': 'mix-blend-mode: luminosity',
  'mix-blend-plus-darker': 'mix-blend-mode: plus-darker',
  'mix-blend-plus-lighter': 'mix-blend-mode: plus-lighter',
  'outline': 'outline-style: solid',
  'outline-dashed': 'outline-style: dashed',
  'outline-dotted': 'outline-style: dotted',
  'outline-double': 'outline-style: double',
  'contain-none': 'contain: none',
  'contain-content': 'contain: content',
  'contain-strict': 'contain: strict',
  'forced-color-adjust-auto': 'forced-color-adjust: auto',
  'forced-color-adjust-none': 'forced-color-adjust: none',
  'outline-none': 'outline: 2px solid transparent;outline-offset: 2px',
  'text-xs': 'font-size: 0.75rem',
  'text-sm': 'font-size: 0.875rem',
  'text-base': 'font-size: 1rem',
  'text-lg': 'font-size: 1.125rem',
  'text-xl': 'font-size: 1.25rem',
  'text-2xl': 'font-size: 1.5rem',
  'text-3xl': 'font-size: 1.875rem',
  'text-4xl': 'font-size: 2.25rem',
  'text-5xl': 'font-size: 3rem',
  'text-6xl': 'font-size: 3.75rem',
  'text-7xl': 'font-size: 4.5rem',
  'text-8xl': 'font-size: 6rem',
  'text-9xl': 'font-size: 8rem',
  'font-thin':	'font-weight: 100',
  'font-extralight':	'font-weight: 200',
  'font-light':	'font-weight: 300',
  'font-normal':	'font-weight: 400',
  'font-medium':	'font-weight: 500',
  'font-semibold':	'font-weight: 600',
  'font-bold':	'font-weight: 700',
  'font-extrabold':	'font-weight: 800',
  'font-black':	'font-weight: 900',
  'stroke-0':	'stroke-width: 0',
  'stroke-1':	'stroke-width: 1',
  'stroke-2':	'stroke-width: 2',
  'indent-0': 'text-indent: 0px',
  'indent-px': 'text-indent: 1px',
  'indent-0.5': 'text-indent: 0.125rem',
  'indent-1': 'text-indent: 0.25rem',
  'indent-1.5': 'text-indent: 0.375rem',
  'indent-2': 'text-indent: 0.5rem',
  'indent-2.5': 'text-indent: 0.625rem',
  'indent-3': 'text-indent: 0.75rem',
  'indent-3.5': 'text-indent: 0.875rem',
  'indent-4': 'text-indent: 1rem',
  'indent-5': 'text-indent: 1.25rem',
  'indent-6': 'text-indent: 1.5rem',
  'indent-7': 'text-indent: 1.75rem',
  'indent-8': 'text-indent: 2rem',
  'indent-9': 'text-indent: 2.25rem',
  'indent-10': 'text-indent: 2.5rem',
  'indent-11': 'text-indent: 2.75rem',
  'indent-12': 'text-indent: 3rem',
  'indent-14': 'text-indent: 3.5rem',
  'indent-16': 'text-indent: 4rem',
  'indent-20': 'text-indent: 5rem',
  'indent-24': 'text-indent: 6rem',
  'indent-28': 'text-indent: 7rem',
  'indent-32': 'text-indent: 8rem',
  'indent-36': 'text-indent: 9rem',
  'indent-40': 'text-indent: 10rem',
  'indent-44': 'text-indent: 11rem',
  'indent-48': 'text-indent: 12rem',
  'indent-52': 'text-indent: 13rem',
  'indent-56': 'text-indent: 14rem',
  'indent-60': 'text-indent: 15rem',
  'indent-64': 'text-indent: 16rem',
  'indent-72': 'text-indent: 18rem',
  'indent-80': 'text-indent: 20rem',
  'indent-96': 'text-indent: 24rem',
  'underline-offset-auto': 'text-underline-offset: auto',
  'underline-offset-0': 'text-underline-offset: 0px',
  'underline-offset-1': 'text-underline-offset: 1px',
  'underline-offset-2': 'text-underline-offset: 2px',
  'underline-offset-4': 'text-underline-offset: 4px',
  'underline-offset-8': 'text-underline-offset: 8px',
  'decoration-auto': 'text-decoration-thickness: auto',
  'decoration-from-font': 'text-decoration-thickness: from-font',
  'decoration-0': 'text-decoration-thickness: 0px',
  'decoration-1': 'text-decoration-thickness: 1px',
  'decoration-2': 'text-decoration-thickness: 2px',
  'decoration-4': 'text-decoration-thickness: 4px',
  'decoration-8': 'text-decoration-thickness: 8px',
  'list-none': 'list-style-type: none',
  'list-disc': 'list-style-type: disc',
  'list-decimal': 'list-style-type: decimal',
  'leading-3': 'line-height: .75rem', 
  'leading-4': 'line-height: 1rem', 
  'leading-5': 'line-height: 1.25rem', 
  'leading-6': 'line-height: 1.5rem', 
  'leading-7': 'line-height: 1.75rem', 
  'leading-8': 'line-height: 2rem', 
  'leading-9': 'line-height: 2.25rem', 
  'leading-10': 'line-height: 2.5rem', 
  'leading-none': 'line-height: 1',
  'leading-tight': 'line-height: 1.25',
  'leading-snug': 'line-height: 1.375',
  'leading-normal': 'line-height: 1.5',
  'leading-relaxed': 'line-height: 1.625',
  'leading-loose': 'line-height: 2',
  'italic': 'font-style: italic',
  'not-italic': 'font-style: normal',
  'ordinal': 'font-variant-numeric: ordinal',
  'slashed-zero': 'font-variant-numeric: slashed-zero',
  'lining-nums': 'font-variant-numeric: lining-nums',
  'oldstyle-nums': 'font-variant-numeric: oldstyle-nums',
  'proportional-nums': 'font-variant-numeric: proportional-nums',
  'tabular-nums': 'font-variant-numeric: tabular-nums',
  'diagonal-fractions': 'font-variant-numeric: diagonal-fractions',
  'stacked-fractions': 'font-variant-numeric: stacked-fractions',
  'tracking-tighter': 'letter-spacing: -0.05em',
  'tracking-tight': 'letter-spacing: -0.025em',
  'tracking-normal': 'letter-spacing: 0em',
  'tracking-wide': 'letter-spacing: 0.025em',
  'tracking-wider': 'letter-spacing: 0.05em',
  'tracking-widest': 'letter-spacing: 0.1em',
  'line-clamp-1': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 1`,
  'line-clamp-2': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2`,
  'line-clamp-3': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3`,
  'line-clamp-4': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 4`,
  'line-clamp-5': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 5`,
  'line-clamp-6': `overflow: hidden;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 6`,
  'line-clamp-none': `overflow: visible;
    display: block;
    -webkit-box-orient: horizontal;
    -webkit-line-clamp: none`,
    'z-0': 'z-index: 0',
  'z-10': 'z-index: 10',
  'z-20': 'z-index: 20',
  'z-30': 'z-index: 30',
  'z-40': 'z-index: 40',
  'z-50': 'z-index: 50',
  'z-auto': 'z-index: auto',
  'object-bottom': 'object-position: bottom',
  'object-center': 'object-position: center',
  'object-left': 'object-position: left',
  'object-left-bottom': 'object-position: left bottom',
  'object-left-top': 'object-position: left top',
  'object-right': 'object-position: right',
  'object-right-bottom': 'object-position: right bottom',
  'object-right-top': 'object-position: right top',
  'object-top': 'object-position: top',
  'columns-1': 'columns: 1',
  'columns-2': 'columns: 2',
  'columns-3': 'columns: 3',
  'columns-4': 'columns: 4',
  'columns-5': 'columns: 5',
  'columns-6': 'columns: 6',
  'columns-7': 'columns: 7',
  'columns-8': 'columns: 8',
  'columns-9': 'columns: 9',
  'columns-10': 'columns: 10',
  'columns-11': 'columns: 11',
  'columns-12': 'columns: 12',
  'columns-auto': 'columns: auto',
  'columns-3xs': 'columns: 16rem',
  'columns-2xs': 'columns: 18rem',
  'columns-xs': 'columns: 20rem',
  'columns-sm': 'columns: 24rem',
  'columns-md': 'columns: 28rem',
  'columns-lg': 'columns: 32rem',
  'columns-xl': 'columns: 36rem',
  'columns-2xl': 'columns: 42rem',
  'columns-3xl': 'columns: 48rem',
  'columns-4xl': 'columns: 56rem',
  'columns-5xl': 'columns: 64rem',
  'columns-6xl': 'columns: 72rem',
  'columns-7xl': 'columns: 80rem',
  'aspect-auto': 'aspect-ratio: auto',
  'aspect-square': 'aspect-ratio: 1 / 1',
  'aspect-video': 'aspect-ratio: 16 / 9',
  'cursor-auto': 'cursor: auto',
  'cursor-default': 'cursor: default',
  'cursor-pointer': 'cursor: pointer',
  'cursor-wait': 'cursor: wait',
  'cursor-text': 'cursor: text',
  'cursor-move': 'cursor: move',
  'cursor-help': 'cursor: help',
  'cursor-not-allowed': 'cursor: not-allowed',
  'cursor-none': 'cursor: none',
  'cursor-context-menu': 'cursor: context-menu',
  'cursor-progress': 'cursor: progress',
  'cursor-cell': 'cursor: cell',
  'cursor-crosshair': 'cursor: crosshair',
  'cursor-vertical-text': 'cursor: vertical-text',
  'cursor-alias': 'cursor: alias',
  'cursor-copy': 'cursor: copy',
  'cursor-no-drop': 'cursor: no-drop',
  'cursor-grab': 'cursor: grab',
  'cursor-grabbing': 'cursor: grabbing',
  'cursor-all-scroll': 'cursor: all-scroll',
  'cursor-col-resize': 'cursor: col-resize',
  'cursor-row-resize': 'cursor: row-resize',
  'cursor-n-resize': 'cursor: n-resize',
  'cursor-e-resize': 'cursor: e-resize',
  'cursor-s-resize': 'cursor: s-resize',
  'cursor-w-resize': 'cursor: w-resize',
  'cursor-ne-resize': 'cursor: ne-resize',
  'cursor-nw-resize': 'cursor: nw-resize',
  'cursor-se-resize': 'cursor: se-resize',
  'cursor-sw-resize': 'cursor: sw-resize',
  'cursor-ew-resize': 'cursor: ew-resize',
  'cursor-ns-resize': 'cursor: ns-resize',
  'cursor-nesw-resize': 'cursor: nesw-resize',
  'cursor-nwse-resize': 'cursor: nwse-resize',
  'cursor-zoom-in': 'cursor: zoom-in',
  'cursor-zoom-out': 'cursor: zoom-out',
};