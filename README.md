# Tailwind CSS Converter

For developpers used to tailwind shortcuts notation, helping them to write CSS faster.

![Demo](https://i.imgur.com/AsJE9I1.gif)

Handle specific values with square bracket

![Demo2](https://i.imgur.com/yav8Cyv.gif)

Tailwind CSS Converter is a Visual Studio Code extension designed to streamline your workflow. It provides convenient features to simplify the conversion of Tailwind CSS shorthand notation to CSS, as well as quick access to a list of available shortcuts.

## Features
- Convert to CSS: Easily convert Tailwind CSS shorthand notation into corresponding CSS styles within your code.
- List of Shortcuts: Quickly access a comprehensive list of available Tailwind CSS shortcuts to enhance your productivity.

## Usage

### Convert to CSS
- Select the text containing Tailwind CSS shorthand notation in your code editor.
- Open the Command Palette by pressing Ctrl+Shift+P (Windows/Linux) or Cmd+Shift+P (Mac).
- Type "[Tailwind -> CSS] : Convert to CSS".
- Press Enter.

You can add a shortcut to this, to be even faster.

Simply add this to your keybindings.json
```
[
  // Your other shortcuts
  {
    "key": "shift+cmd+r",
    "command": "tailwind-to-css.convertTailwindToCSS"
  }
]
```

### List of Shortcuts
- Open the Command Palette by pressing Ctrl+Shift+P (Windows/Linux) or Cmd+Shift+P (Mac).
- Type "[Tailwind -> CSS] : All available shortcuts".
- Press Enter.

## Example
Given the input:

```css
bg-blue-500 p-4 flex
```
The output will be:

```css
background-color: #3b82f6;
padding: 1rem;
display: flex;
```

## Contributing
Contributions are welcome! If you have any ideas for new features, improvements, or bug fixes, please open an issue or submit a pull request on [Gitlab](https://gitlab.com/totol.toolsuite/vscode-tailwind-to-css).

## License
This extension is licensed under the MIT License.


Enjoy!